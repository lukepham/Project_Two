
package com.revature.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.revature.model.User;


//public class EmailUtil {
//
//	public static void sendResetEmail(User user){
//		ResetEmailThread t = new ResetEmailThread();
//		t.setUser(user);
//		
//		t.start();
//	}
//}

public class ResetEmailThread extends Thread {
	


	public ResetEmailThread(User user) {
		super();
		this.user = user;
	}


	private User user;
	
	@Override
	public void run() {
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(FinalUtil.PROJECT_EMAIL_ADDRESS,FinalUtil.PROJECT_EMAIL_PASSWORD);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(FinalUtil.PROJECT_EMAIL_ADDRESS));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(user.getEmail()));
			message.setSubject(user.getUsername() + " Postage Password Reset");
			message.setText("Hello " + user.getFirstName() + ", your new password is " + user.getPassword());

			Transport.send(message);

			//System.out.println("Done");

		} catch (MessagingException e) {
			//update for log4j
		}
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}