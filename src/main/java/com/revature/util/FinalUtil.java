package com.revature.util;

/**
 * General utility class for constants.
 * @author Justin Priester
 *
 */
public class FinalUtil {
	
	public final static int SUCCESS_CODE = 200;
	public final static String SUCCESS_MESSAGE = "SUCCESS";
	
	public final static int USERNAME_AVAILABLE_CODE = 600;
	public final static String USERNAME_AVAILABLE_MESSAGE = "USERNAME AVAILABLE";
	
	public final static int USERNAME_TAKEN_CODE = 601;
	public final static String USERNAME_TAKEN_MESSAGE = "USERNAME TAKEN";
	
	public final static int MAX_POST_LIST_SIZE = 10;
	
	public final static String DEFAULT_PROFILE_PICTURE = "https://s3.us-east-2.amazonaws.com/this-is-dirks-test-bucket789/defaultUser.png";
	
	public final static String PROJECT_EMAIL_ADDRESS = "projTwoRev";
	public final static String PROJECT_EMAIL_PASSWORD = "postagePassword";

	
	public final static String ALPHANUMERIC = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	public final static int GENERATED_PASSWORD_LENGTH = 8;

}
