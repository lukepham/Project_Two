package com.revature.service;

import java.util.List;

import com.revature.model.Post;
import com.revature.model.User;
/**
 * Business Logic for Posts
 * Called By Request Mapper for MVC control flow
 * @author Scott Wolf
 *
 */
public interface PostService {
	
	/**
	 * Gets all posts after the initial posts 
	 * @param lastPost - last post in feed, used to get next posts
	 * @return - list of ten posts
	 */
	public List<Post> retrieveAllPosts(Post lastPost);
	
	/**
	 * Gets all posts from a certain user
	 * @param user - user to get posts of 
	 * @return - lists of posts by user
	 */
	public List<Post> retrievePostsByUsername(User user);
	
	/**
	 * Calls Repository to create a new post
	 * @param newPost - post to be submitted, with content, image(optional), creator id
	 */
	public void submitPost(Post newPost);
	
	/**
	 * Calls PostRepo to delete a post from DB
	 * @param post - post to be deleted
	 */
	public void deletePost(Post post);
	
	/**
	 * If post is unliked, likes post
	 * Otherwise, unlikes post
	 * @param post - post to be like or unliked
	 * @param user - user doing above action
	 */
	public void toggleLike(Post post, User user);
	
	/**
	 * Gets the first ten posts by timestamp, used to fill feed initially
	 * @return - list of first ten posts by timestamp
	 */
	public List<Post> retrieveAllInitialPosts();
}
