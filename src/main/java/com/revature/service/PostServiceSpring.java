package com.revature.service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revature.model.Post;
import com.revature.model.User;
import com.revature.repository.PostRepository;

@Service("postService")
public class PostServiceSpring implements PostService {

	@Autowired
	private PostRepository postRepository;

	public PostServiceSpring(){};

	@Transactional
	@Override
	public List<Post> retrieveAllPosts(Post lastPost) {
		return postRepository.selectAll(lastPost);
	}
	
	@Transactional
	@Override
	public List<Post> retrievePostsByUsername(User user) {
		return postRepository.selectByUsername(user);
	}

	@Transactional
	@Override
	public void submitPost(Post newPost) {
		Calendar calendar = Calendar.getInstance();
		Timestamp currentTimestamp = new java.sql.Timestamp(calendar.getTime().getTime());
		newPost.setCreatedTime(currentTimestamp);
		postRepository.insert(newPost);
	}

	@Transactional
	@Override
	public void deletePost(Post post) {
		postRepository.delete(post);
	}
	
	@Transactional
	@Override
	public void toggleLike(Post post, User user){
		if(post.getUserWhoLiked().contains(user)){
			postRepository.toggleLikeOff(post, user);
		} else{
			postRepository.toggleLikeOn(post, user);
		}
	}
	
	@Override
	public List<Post> retrieveAllInitialPosts() {
		return postRepository.selectAllInitial();
	}
}
