package com.revature.repository;

import java.util.List;
import com.revature.model.Post;
import com.revature.model.User;

/**
 * Called by Service, provides access to Data in Database
 * @author Scott Wolf
 *
 */
public interface PostRepository {
	
	//Used to show all posts on the feed
	//Investigate limited call to only get ten at a time
	//Dont want to transfer all posts across the network at one time
	/**
	 * Returns list of posts, unfiltered by user
	 * @param lastPost- last post in feed
	 * @return- list of first ten posts
	 */
	public List<Post> selectAll(Post lastPost);
	
	//Used to view certain users posts
	/**
	 * Gets list of post from a single user
	 * @param user- user to get posts of 
	 * @return
	 */
	public List<Post> selectByUsername(User user);
	
	//Create a new post
	/**
	 * Creates new post in database
	 * @param newPost - contains content, user who created, image
	 */
	public void insert(Post newPost);
	
	//Delete a post
	/**
	 * Takes in a post, removes it from Database
	 * @param postToDelete - post to delete
	 */
	public void delete(Post postToDelete);
	
	/**
	 * Takes post, and user who likes it, increments post like count
	 * Adds post to list of posts users have like
	 * Adds user to list of user who have liked post
	 * @param post - Post that is being liked
	 * @param user - User who likes post
	 */
	public void toggleLikeOn(Post post, User user);
	
	/**
	 * Takes post, and user who dislikes it, decrements post like count
	 * Only can be called if user has already liked a post
	 * Removes post to list of posts users have like
	 * Removes user to list of user who have liked post
	 * @param post - Post that is being unliked
	 * @param user - User who unlikes post 
	 */
	public void toggleLikeOff(Post post, User user);
	
	
	/**\
	 * Used on page loading to initially populate the feed
	 * @return - Returns first ten posts in DB
	 * 
	 */
	public List<Post> selectAllInitial();
	
}

