package com.revature.ajax;

/**
 * A message meant to be unmarshalled into a JSON.
 * @author Justin Priester
 *
 */
public class AjaxMessage {
	
	private int code;
	private String message;
	
	public AjaxMessage(int code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public int getCode() {
		return this.code;
	}
	
	public String getMessage() {
		return this.message;
	}
}
