package com.revature.model;

/**
 * POJO that provides misc. User data.
 * @author Justin Priester
 *
 */
public class UserMisc {
	
	private String name;

	public UserMisc() {
		name = "";
	}

	public UserMisc(String name) {
		this.name = name.toUpperCase();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name.toUpperCase();
	}
	
}