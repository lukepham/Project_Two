package com.revature.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * POJO representing a user's account on the website.
 * @author Justin Priester
 *
 */
@Entity
@Table(name="USER_T",
uniqueConstraints ={@UniqueConstraint (columnNames = {"U_USERNAME"})})
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_SEQ")
	@SequenceGenerator(name="USER_SEQ", sequenceName="USER_SEQ", allocationSize=1)
	@Column(name="U_ID")
	private int id;
	
	@Column(name="U_USERNAME")
	private String username;
	
	@Column(name="U_PASSWORD")
	private String password;
	
	@Column(name="U_EMAIL")
	private String email;
	
	@Column(name="U_FIRSTNAME")
	private String firstName;
	
	@Column(name="U_LASTNAME")
	private String lastName;
	
	@Column(name="U_PROFILEPICTURE")
	private String profilePicture; 
	
	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Post> likedPostList;
	
	public User(){};
	
	public User(String username){
		this.username = username;
	}
	
	public User(String username, String password){
		this.username = username;
		this.password = password;
	}

	public User(int id, String username, String password, String email, String firstName, String lastName,
			String profilePicture, List<Post> likedPostList) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.profilePicture = profilePicture;
		this.likedPostList = likedPostList;
		
		if (this.firstName != null) {
			this.firstName = this.firstName.toUpperCase();
		}
		
		if (this.lastName != null) {
			this.lastName = this.lastName.toUpperCase();
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public List<Post> getLikesPostId() {
		return likedPostList;
	}

	public void setLikesPostId(List<Post> likedPostList) {
		this.likedPostList = likedPostList;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
		
		if (this.firstName != null) {
			this.firstName = this.firstName.toUpperCase();
		}
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
		
		if (this.lastName != null) {
			this.lastName = this.lastName.toUpperCase();
		}
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", email=" + email
				+ ", profilePicture=" + profilePicture + ", likesPostId=" + likedPostList + "]";
	}
	
}
