package com.revature.model;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.transaction.Transactional;

/**
 * POJO for social network posts
 * @author Scott Wolf
 *
 */
@Entity
@Table(name="POST")
public class Post {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="POST_SEQ")
	@SequenceGenerator(name="POST_SEQ", sequenceName="POST_SEQ", allocationSize=1)
	@Column(name="P_ID")
	private int id;
	
	@Column(name="P_CONTENT")
	private String content;
	
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "likedPostList")	
	private List<User> userWhoLiked;
	
	@OneToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	private User postCreator;
	
	@Column(name="P_IMAGE")
	private String postImage;
	
	@Column(name="P_LIKECOUNT")
	private int likeCount;
	
	@Column(name="P_CREATIONDATE")
	private Timestamp createdTime;

	public Post(){}

	public Post(int id, String content, List<User> userWhoLiked, User postCreator, 
			String postImage, int likeCount, Timestamp createdTime) {
		super();
		this.id = id;
		this.content = content;
		this.userWhoLiked = userWhoLiked;
		this.postCreator = postCreator;
		this.postImage = postImage;
		this.likeCount = likeCount;
		this.createdTime = createdTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<User> getUserWhoLiked() {
		return userWhoLiked;
	}

	public void setUserWhoLiked(List<User> userWhoLiked) {
		this.userWhoLiked = userWhoLiked;
	}

	public User getPostCreator() {
		return postCreator;
	}

	public void setPostCreator(User postCreator) {
		this.postCreator = postCreator;
	}

	public String getPostImage() {
		return postImage;
	}

	public void setPostImage(String postImage) {
		this.postImage = postImage;
	}

	public int getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(int likeCount) {
		this.likeCount = likeCount;
	}
	
	public Timestamp getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	@Override
	@Transactional
	public String toString() {
		return "Post [id=" + id + ", content=" + content + ", userWhoLiked=" + userWhoLiked + ", postCreator="
				+ postCreator + ", postImage=" + postImage + ", likeCount=" + likeCount + "]";
	};
}
