//The logic for login/registering page routing

var app = angular.module("index", ["ngRoute", "ngCookies","controllers", "directives"]);
var directives = angular.module('directives', []);
var controllers = angular.module('controllers', []);
app.service('LastPost', function() {
	  return {
		  id : ''
	  };
});
app.service('LoggedUser', function() {
	  return {
		  id : '',
	      username : '',
	      firstName : '',
	      lastName : '',
	      email : '',
	      image : '',
	      likes : []
	  };
});

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "login.html"
    })
    .when("/login", {
        templateUrl : "login.html"
    })
    .when("/register", {
        templateUrl : "register.html"
    })
    .when("/reset", {
        templateUrl : "ForgottenPassword.html"
    })
    .when("/home", {
        templateUrl : "home.html"
    })
});

//THIS IS THE CONTROLLER THAT HANDLES PASSWORD RESETS
app.controller('reset', function($scope, $http, $location, $cookies) {
	$scope.sendEmail = () => {
		console.log($cookies.get('username') + "=?" + $scope.reset.username);
		
		if($scope.reset.username == $cookies.get('username')){
			$http.post('http://localhost:8080/Project_Two/resetUserPassword.app', $scope.reset)
			.then((successResponse) => {
				console.log(successResponse.data);
				
			});
		}
	}

});

//THIS IS THE CONTROLLER THAT HANDLES LOGGING IN AN EXISTING USER
app.controller('login', function($scope, $http, $cookies, $location) {
	$scope.loginUser = () => {
		console.log($scope.login);
		console.log("Logging in");
		$http.post('http://localhost:8080/Project_Two/login.app', $scope.login)
		.then((successResponse) => {
			console.log("After ajax request");
			console.log(successResponse.data);
			//Redirect to home in angular
			if(successResponse.data.username != null){	
				console.log("Successful login credentials");
				$cookies.put('id', successResponse.data.id);
				$cookies.put('username', successResponse.data.username);
				$cookies.put('firstName', successResponse.data.firstName);
				$cookies.put('lastName', successResponse.data.lastName);
				$cookies.put('email', successResponse.data.email);
				$cookies.put('image', successResponse.data.profilePicture);
				$cookies.put('likes', successResponse.data.likesPostId);
				$location.path("/home");
			}else{console.log("Unsuccessful login credentials");}
			
		})
	}	
});


/*
 * THIS IS THE CONTROLLER THAT HANDLES REGISTERING A NEW USER
 * THIS IS THE CONTROLLER THAT HANDLES REGISTERING A NEW USER
 * THIS IS THE CONTROLLER THAT HANDLES REGISTERING A NEW USER
 */
app.controller('register', function($scope, $http, $location) {
	$scope.checkUsername = () => {
		$scope.uniqueError = "";
		$scope.fieldsError = "";
		$scope.passwordError = "";
		console.log("check username");
		$http.post('http://localhost:8080/Project_Two/checkUniqueUsername.app', $scope.info)
		.then((successResponse) => {
			console.log(successResponse.data);
			
			if(successResponse.data.code == 600){
				console.log("username is unique");
					if($scope.info.password == document.getElementById("passwordCheck").value){
						console.log("passwords match");
						
						$http.post('http://localhost:8080/Project_Two/registerUser.app', $scope.info)
						.then((successResponse) => {
							$location.path("/login");
						})
					}
			}
			if(successResponse.data.code == 601){
				$scope.uniqueError = "Username is taken";
				console.log("We can NOT make this username");
				console.log($scope.uniqueError);
			}
			if($scope.info.password != document.getElementById("passwordCheck").value){
				$scope.passwordError = "Passwords must match";
				console.log("Passwords didn't match");
				console.log($scope.passwordError);
			}
				
		})
	}
});


//THIS IS THE HOME CONTROLLER 
app.controller('homeController', function(LoggedUser, LastPost, $scope, $http, $cookies, $location) {
	/*
	 * Beginning of initializing home page:
	 * we set the logged user Object to the
	 *  cookies set from login
	 *  
	 *  In this area we should also have an 
	 *  ajax call that will get the first set
	 *  of user posts to set on the page
	 *  
	 *  We may also want to have a scope variable
	 *  that contains the id of the last post in 
	 *  the currently viewed list
	 */
	LoggedUser.id = $cookies.get('id');
	LoggedUser.username = $cookies.get('username');
	LoggedUser.firstName = $cookies.get('firstName');
	LoggedUser.lastName = $cookies.get('lastName');
	LoggedUser.email = $cookies.get('email');
	LoggedUser.image = $cookies.get('image'); 
	LoggedUser.likes = $cookies.get('likes');
	$scope.user = LoggedUser;
	/*
	 * End of Initializing 
	 */
	
	
	//some hopeless attempt to get an initial set without scotts endpoint
	$http.post('http://localhost:8080/Project_Two/getInitialPosts.app')
	.then((successResponse) => {
		$scope.postList = successResponse.data;
		console.log(successResponse.data);
		LastPost.id = successResponse.data[9].id;
		console.log(LastPost.id);
	});

	$scope.morePosts = () => {
		console.log("The ID of last post is: " + LastPost.id);
		var getMore = { 
				postImage: null, 
				content: null, 
				likeCount: null, 
				postCreator: null,
				id: LastPost.id,
				userWhoLiked: null};
		console.log(getMore);
		$http.post('http://localhost:8080/Project_Two/getPosts.app', getMore)
		.then((successResponse) => {
			if(successResponse.data.length > 0){
				LastPost.id = successResponse.data[successResponse.data.length-1].id;
			}
			else{
				LastPost.id = 84;
			}
			console.log("The ID of last post is: " + LastPost.id);
			
			$scope.postList = successResponse.data;
			scroll(0,0); //this is a really crappy way to do it
		});
	}

	/*
	 * THIS IS FOR UPDATING A USERS IMAGE, IT WILL CALL THE BASE UPLOAD 
	 */
	$scope.userImageUpload = () => {
		if($scope.upload(document.getElementById("newUserPic").files[0]) == true && document.getElementById("newUserPic").files[0] != null){
			var filename = document.getElementById("newUserPic").value.replace(/^.*[\\\/]/, '');
			filename = "https://s3.us-east-2.amazonaws.com/this-is-dirks-test-bucket789/".concat(filename);
			console.log("full path: " + filename);
			var updateObject = {id: $scope.user.id, profilePicture: filename};
			$http.post('http://localhost:8080/Project_Two/updateUserProfilePicture.app', updateObject)
			.then((successResponse) => {
				$scope.user.image = filename;
				$cookies.put('image', filename);
			});
		}
	}
	
	//Dirk's impementation of submitting a post, leverages upload() function for adding an image
	$scope.submitPost = () => {
		if($scope.upload(document.getElementById("postImage").files[0]) == true && document.getElementById("postImage").files[0] != null){
			console.log("this is their picture" + document.getElementById("newUserPic").value);
			var filename = document.getElementById("postImage").value.replace(/^.*[\\\/]/, '');
			filename = "https://s3.us-east-2.amazonaws.com/this-is-dirks-test-bucket789/".concat(filename);
			var postObject = {postCreator: $scope.user, postImage: filename, content: $scope.userPost.content, likeCount: "", id: "", userWhoLiked: []};
			$http.post('http://localhost:8080/Project_Two/insertPost.app', postObject)
			.then((successResponse) => {

			});
			//Then we get the top stack of posts and set the feed to the newest stuff
			$http.post('http://localhost:8080/Project_Two/getInitialPosts.app')
			.then((successResponseInner) => {
				$scope.postList = successResponseInner.data;
				console.log(successResponseInner.data);
				LastPost.id = successResponseInner.data[9].id;
				console.log(LastPost.id);
			});
			
		}else{
			var postObject = {postCreator: $scope.user, postImage: "", content: $scope.userPost.content, likeCount: "", id: "", userWhoLiked: []};
			$http.post('http://localhost:8080/Project_Two/insertPost.app', postObject)
			.then((successResponse) => {});
			
			//Then we get the top stack of posts and set the feed to the newest stuff
			$http.post('http://localhost:8080/Project_Two/getInitialPosts.app')
			.then((successResponse) => {
				$scope.postList = successResponse.data;
				console.log(successResponse.data);
				LastPost.id = successResponse.data[9].id;
				console.log(LastPost.id);
			});
		}
	}
	
	//this will upload an object to S3 we use this function for both update user image and to submit a post with an image attached
	$scope.upload = (file) => {
		  // Configure The S3 Object 
		  AWS.config.update({ accessKeyId: 'AKIAIW52XRW3NFT4OYJQ', secretAccessKey: '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', signatureVersion: 'v4' });
		  AWS.config.region = 'us-east-2';
		  var bucket = new AWS.S3({ params: { Bucket: 'this-is-dirks-test-bucket789' } });
		  if(file != null) {
			  console.log("After bucket creation: ", typeof(bucket));
			  console.log("we have a file: " + file.name);
		  	var params = { Key: file.name, ContentType: file.type, Body: file, ServerSideEncryption: 'AES256' };
		    var result = bucket.putObject(params, function(err, data) {
		      if(err) {
		        // There Was An Error With Your S3 Config
		        console.log(err.message);
		        return false;
		      }
		      else {
		        // Success!
		    	console.log('Upload Done');
	    	    return true;
		        
		      }
		    })
		    .on('httpUploadProgress',function(progress) {
		          // Log Progress Information
		          console.log(Math.round(progress.loaded / progress.total * 100) + '% done');
		        });
		    return true;
		  }
	}
	
	
	/*
	 * 
	 * THIS IS THE BEGINNING OF STEPHANIES JAVASCRIPT LOGIC
	 * I MOVED THIS FROM HOME.JS TO INDEX.JS, IT LOOKS LIKE 
	 * EVERYTHING IS WORKING AS INTENDED NOW FOR THE HOME PAGE
	 * HOPEFULLY WORKING IN A SINGLE JS FILE WON'T BE THE DEATH
	 * OF US
	 */
	$scope.getSearch = () => {
		$scope.userList = {};
		console.log("getting users from search user: " + $scope.searchUser);
		$http.post(domain + "searchUsers.app", $scope.searchUser)
		.then((successResponse) => {
			$scope.userList = successResponse.data;
		})
	}
	
	//this will throw a bad request error because getPosts.app needs a last post sent as a parameter
	$scope.getAllPosts = () => {
		$scope.allPostList = {};
		console.log("Selecting all posts");
		$http.post(domain + "getPosts.app", blankPost)
		.then((successResponse) => {
			console.log("successResponse before");
			$scope.allPostList = successResponse.data;
		})
	}
	
	$scope.likePost = (inPost, inUser) => {
		$scope.postList = {};
		console.log("select a post works " + inUser.id + " Post ID: " + inPost.id);
		//below needs to be changed
		$http.post(domain + "toggleLike.app", inPost, inUser)
		.then((successResponse) => {
			console.log("successResponse before");
			$scope.likePost = successResponse.data;
		})
	}
	
	$scope.getSelectedUserPosts = (inUser) => {
		$scope.postList = {};
		console.log("Selected User Post after Works. user: " + inUser.id);
		$http.post(domain + "getUserPost.app", inUser)
		.then((successResponse) => {
			console.log("successResponse before");
			$scope.postList = successResponse.data;
		})
	}
	/*
	 * I Commented this out because my implementation covers a post with and without an image
	 * we'll leave it here if it's necessary for something I missed
	 * -Dirk
	 */
//	$scope.submitPost = () => {
//		$scope.userPost = {};
//		console.log("Inserting user post");
//		//put user post
//		//insert the image, the post, the user creator
//		console.log("Displaying user post: " + $scope.userPost);
//		$http.post(domain + "insertPost.app", $scope.userPost)
//		.then((successResponse) => {
//			$scope.addedPost = successResponse.data;
//		})
//	}
	$scope.editInfo = () => {
		//im not sure what to do here
		//i want to change the values to inputs then save what the user has changed
		$scope.editUser = {}
		console.log("Editing user info");
		console.log("first name = "+ $scope.userFirstName.value);
		//needs to be persisted in the database and then persisted on the page
		
		//updateUserInformation.app
	}
	
	/*
	 * Implemented in Dirks code set
	 */
//	$scope.editPicture = () => {
//		//updateUserProfilePicture.app
//	}
});

