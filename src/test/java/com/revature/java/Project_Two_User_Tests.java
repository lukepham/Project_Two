/*package com.revature.java;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.revature.model.User;
import com.revature.model.UserMisc;
import com.revature.service.PostService;
import com.revature.service.UserService;
import com.revature.util.AppContextUtil;
import com.revature.util.FinalUtil;
import com.revature.util.HashUtil;

public class Project_Two_User_Tests {
	
	public PostService postService;
	public UserService userService;
	
	@Before
	public void setUp() throws Exception {
		//postService = AppContextUtil.getPostService();
		userService = AppContextUtil.getUserService();
	}
	
	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void uniqueUsername(){
		assertTrue(userService.isUniqueUsername(new User("FakeUsername")));
	}

	@Test
	public void loginTestValidUser() {
		//new user with posted username and password
		User testUser = new User("username","password");
		
		//select full user based on given username
		userService.resetUser(testUser);
		
		User trustyUser = new User("username", "password");
		User dbUser = userService.login(trustyUser);
		
		//check if username given matches password in db
		assertTrue(testUser.getUsername().equals(dbUser.getUsername()));
	}
	
	@Test
	public void loginTestInvalidUser() {
		//new user with posted username and password
		User testUser = new User("username","password");
		
		//select full user based on given username
		userService.resetUser(testUser);
		
		User scamUser = new User("username", "1234");
		User dbUser = userService.login(scamUser);
		
		//check if username given matches password in db
		assertFalse(testUser.getUsername().equals(dbUser.getUsername()));
	}
	
	@Test
	public void updateUserInformation(){
		//updates user info
		//create user
		User originalUser = new User(0, "updateOriginalTest", "test", "test", "test",
				"test", null, null);
		//create new updated user
		User updatedUser = new User(0, "updateTest", "test", "updateTest", "updateTest",
				"updateTest", null, null);
		//insert user
		userService.unregisterUser(updatedUser);
		userService.resetUser(originalUser);
		//make sure user has all info
		originalUser = userService.retrieveUser(originalUser);
		updatedUser.setId(originalUser.getId());
		//update original user
		userService.updateUserInformation(updatedUser);
		//get new values into original user
		originalUser = userService.retrieveUser(updatedUser);
		//make sure original user matches updated user
		assertTrue(originalUser.getUsername().equals(updatedUser.getUsername()));
		assertTrue(originalUser.getEmail().equals(updatedUser.getEmail()));
		assertTrue(originalUser.getFirstName().equals(updatedUser.getFirstName()));
		assertTrue(originalUser.getLastName().equals(updatedUser.getLastName()));
	}
	
	@Test
	public void updateUserPassword(){
		//updates user info
		//create user
		User originalUser = new User(0, "updateTest2", "test", "test", "test",
				"test", null, null);
		//create new updated user
		User updatedUser = new User(0, "updateTest2", "updateTest", "test", "test",
				"test", null, null);
		//insert user
		userService.unregisterUser(updatedUser);
		userService.resetUser(originalUser);
		//make sure user has all info
		originalUser = userService.retrieveUser(originalUser);
		updatedUser.setId(originalUser.getId());
		//update original user
		userService.updateUserPassword(updatedUser);
		//get new values into original user
		originalUser = userService.retrieveUser(updatedUser);
		//make sure original user matches updated user
		assertTrue(originalUser.getPassword().equals(HashUtil.hash(updatedUser.getPassword())));
	}
	
	@Test
	public void updateUserProfilePicture(){
		//updates user info
		//create user
		User originalUser = new User(0, "updateTest3", "test", "test", "test",
				"test", "bing.com", null);
		//create new updated user
		User updatedUser = new User(0, "updateTest3", "test", "test", "test",
				"test", "google.com", null);
		//insert user
		userService.unregisterUser(updatedUser);
		userService.resetUser(originalUser);
		//make sure user has all info
		originalUser = userService.retrieveUser(originalUser);
		updatedUser.setId(originalUser.getId());
		//update original user
		userService.updateUserProfilePicture(updatedUser);
		//get new values into original user
		originalUser = userService.retrieveUser(updatedUser);
		//make sure original user matches updated user
		assertTrue(originalUser.getProfilePicture().equals(updatedUser.getProfilePicture()));
	}
	
	@Test
	public void insertUser() {
		User user = new User(0, "insertTest", "test", "test", "test",
				"test", null, null);
		userService.resetUser(user);
		assertTrue(userService.retrieveUser(user).getUsername().equals(user.getUsername()));
	}
	
	@Test
	public void deleteUser() {
		User user = new User(0, "deleteTest", "test", "test", "test",
				"test", null, null);
		userService.resetUser(user);
		assertTrue(userService.retrieveUser(user).getUsername().equals(user.getUsername()));
		userService.unregisterUser(user);
		User insertedUser = userService.retrieveUser(user);
		assertNotNull(insertedUser);
		assertFalse(user.getUsername().equals(insertedUser.getUsername()));
	}
	
	//-Select Single User: Builds off select user. Takes verified username, gets complete user info
	@Test
	public void selectSingleUser() {
		User user = new User(0, "insertTest", "test", "test", "test",
				"test", null, null);
		userService.resetUser(user);
		assertNotNull(userService.retrieveUser(user));
	}
	
	@Test
	public void findUsers() {
		List<User> users = new ArrayList<>();
		users.add(new User(0, "findTest1", "test", "test", "testa",
				"test1", null, null));
		users.add(new User(0, "findTest2", "test", "test", "testab",
				"test12", null, null));
		users.add(new User(0, "findTest3", "test", "test", "testabc",
				"test123", null, null));
		users.add(new User(0, "findTest4", "test", "test", "test1",
				"testa", null, null));
		users.add(new User(0, "findTest5", "test", "test", "test12",
				"testab", null, null));
		users.add(new User(0, "findTest6", "test", "test", "test123",
				"testabc", null, null));
		users.add(new User(0, "findTest7", "test", "test", "testbobby",
				"testmartin", null, null));
		
		for (User user: users) {
			userService.resetUser(user);
		}
		
		assertTrue(userService.findUsers(new UserMisc("testa")).size() == 6);
		assertTrue(userService.findUsers(new UserMisc("testab")).size() == 4);
		assertTrue(userService.findUsers(new UserMisc("testabc")).size() == 2);
		assertTrue(userService.findUsers(new UserMisc("test1")).size() == 6);
		assertTrue(userService.findUsers(new UserMisc("test12")).size() == 4);
		assertTrue(userService.findUsers(new UserMisc("test123")).size() == 2);
		assertTrue(userService.findUsers(new UserMisc("testabc test123")).size() == 2);
		assertTrue(userService.findUsers(new UserMisc("test123 testabc")).size() == 2);
		assertTrue(userService.findUsers(new UserMisc("testbobby testmartin")).size() == 1);
		assertTrue(userService.findUsers(new UserMisc("testmartin testbobby")).size() == 1);
	}
	
	@Test
	public void resetUserPassword() {
		User user = new User(0, "resetPasswordTest", "test", "test", "test",
				"test", null, null);
		
		userService.resetUser(user);
		
		User resettedUser = userService.resetUserPassword(user);
		
		assertTrue(resettedUser.getPassword().length() == FinalUtil.GENERATED_PASSWORD_LENGTH);
	}
	
}*/